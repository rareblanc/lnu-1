window.onscroll = function showheader() {
    var header = document.querySelector('.header__boss')
    if (window.pageYOffset > 400) {
        header.classList.add('header__fixed');
    }
    else {
        header.classList.remove('header__fixed');
    }
}
